--[[---------------------------------------------------------------------------------
    Localisation for zhCN
----------------------------------------------------------------------------------]]

local L = AceLibrary("AceLocale-2.0"):new("Clique")

L:RegisterTranslations("zhCN", function()
    return {
        RANK						= "等级",
        RANK_PATTERN					= "等级 (%d+)",
        MANA_PATTERN                = "(%d+)法力值",
        HEALTH_PATTERN              = "(%d+)到(%d+)",

        RANK_FORMAT					= "等级 %d",
	SPELL_FORMAT 					= "%s(等级 %d)",
        
        ["Lesser Heal"]             = "次级治疗术",
        ["Heal"]                    = "治疗术",
        ["Greater Heal"]            = "强效治疗术",
        ["Flash Heal"]              = "快速治疗",
        ["Healing Touch"]           = "治疗之触",
        ["Regrowth"]                = "愈合",
        ["Healing Wave"]            = "治疗波",
        ["Lesser Healing Wave"]     = "次级治疗波",
        ["Holy Light"]              = "圣光术",
        ["Flash of Light"]          = "圣光闪现",

        DUAL_HOLY_SHOCK             = "神圣震击",
        DUAL_MIND_VISION            = "心灵视界",
        
        FREE_INNER_FIRE             = "心灵之火",

        CURE_CURE_DISEASE           = "祛病术",
        CURE_ABOLISH_DISEASE        = "驱除疾病 ",
        CURE_PURIFY                 = "纯净术",
        CURE_CLEANSE                = "清洁术",
        CURE_DISPEL_MAGIC           = "驱散魔法",
        CURE_CURE_POISON            = "消毒术",
        CURE_ABOLISH_POISON         = "驱毒术",
        CURE_REMOVE_LESSER_CURSE    = "解除次级诅咒",
        CURE_REMOVE_CURSE           = "解除诅咒",

        BUFF_PWF                    = "真言术：韧",
        BUFF_PWS                    = "真言术：盾",
        BUFF_SP                     = "防护暗影",
        BUFF_DS                     = "神圣之灵",
        BUFF_RENEW                  = "恢复",
        BUFF_MOTW                   = "野性印记",
        BUFF_THORNS                 = "荆棘术",
        BUFF_REJUVENATION           = "回春术",
        BUFF_REGROWTH               = "愈合",
        BUFF_AI                     = "奥术智慧",
        BUFF_DM                     = "魔法抑制",
        BUFF_AM                     = "魔法增效",
        BUFF_BOM                    = "力量祝福",
        BUFF_BOP                    = "保护祝福",
        BUFF_BOW                    = "智慧祝福",
        BUFF_BOS                    = "庇护祝福",
        BUFF_BOL                    = "光明祝福",
        BUFF_BOSFC                  = "牺牲祝福", 

        DEFAULT_FRIENDLY            = "默认友好",
        DEFAULT_HOSTILE             = "默认敌对",

        BINDING_NOT_DEFINED         = "未指定的快捷键",
        COULD_NOT_FIND_MODULE       = "找不到名为 \"%s\" 的模块",
        COULD_NOT_FIND_FRAME        = "启用模块 \"%s\" 时找不到框架 \"%s\"",
        PLUGIN_NOT_PROPER           = "\"%s\" 的插件似乎没有框架列表或启用功能。",
        NO_UNIT_FRAME               = "无法确定与 \"%s\" 对应的哪个单元",
        CUSTOM_SCRIPT               = "自定义摸块",
        ERROR_SCRIPT                = "|cff00ff33Clique: 有一个 |cff00ff33模块|r |cffff3333出错:|r %s",
        ENABLED_MODULE              = "|cff00ff33Clique: 启用模块|r %s" ,

        TT_DROPDOWN                 = "你现在正在编辑 \"set\" ",
        TT_LIST_ENTRY               = "双击编辑，或单击选择",
        TT_DEL_BUTTON               = "单击以删除所选条目",
        TT_MAX_BUTTON               = "点击更改此法术总是以最高等级技能释放",
        TT_NEW_BUTTON               = "创建新的用户模块",
        TT_EDIT_BUTTON              = "编辑点击施法项目",
        TT_OK_BUTTON                = "退出Clique配置屏幕",
        TT_EDIT_BINDING             = "Perform a click-cast here to change the binding",
        TT_NAME_EDITBOX             = "自定义模块的名称",
        TT_SAVE_BUTTON              = "保存你的更改",
        TT_CANCEL_BUTTON            = "取消更改",
        TT_TEXT_EDITBOX             = "在此处键入自定义LUA代码",
        TT_PULLOUT_TAB              = "点击打开/关闭Clique配置屏幕" ,
    }
end)